//
//  main.swift
//  Battery Life Indicator Helper
//
//  Created by Alexandr Sieroshtan on 19.11.2020.
//  Copyright © 2020 Alexandr Sieroshtan. All rights reserved.
//

import Cocoa

let delegate = AutoLauncherAppDelegate()
NSApplication.shared.delegate = delegate
_ = NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
