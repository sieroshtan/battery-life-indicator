//
//  Int.swift
//  Battery
//
//  Created by Alexandr Sieroshtan on 17.11.2020.
//

import Foundation

public extension Int {
    var digitCount: Int {
        var n = self
        var count = 0
        while n > 0 {
            count += 1
            n /= 10
        }
        return count
    }

    var readableTimeInMin: String {
        let hour = self / 60
        let minute = self % 60

        return String(format: "%d:%02d", arguments: [hour, minute])
    }
    
    var formatCapacity: String {
        return String(format: "%d mAh", arguments: [self])
    }
}
