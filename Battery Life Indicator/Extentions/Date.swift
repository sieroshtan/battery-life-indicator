//
//  Date.swift
//  Battery Life Indicator
//
//  Created by Alexandr Sieroshtan on 20.11.2020.
//  Copyright © 2020 Alexandr Sieroshtan. All rights reserved.
//

import Foundation

public extension Date {
    func formatted() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.locale = Locale.current
        
        return dateFormatter.string(from: self)
    }
}
