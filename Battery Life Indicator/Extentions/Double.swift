//
//  Double.swift
//  Battery
//
//  Created by Alexandr Sieroshtan on 17.11.2020.
//

import Foundation

public extension Double {
    var percentageString: String {
        return (isNaN || isInfinite) ? "N/A" : String(format: "%.0f%%", self * 100)
    }
    
    func formatTemp(unit: TemperatureUnit = .celsius) -> String {
        return String(format: "%.0f\(unit.rawValue)", self)
    }
}
