//
//  AppDelegate.swift
//  Battery Time Indicator Helper
//
//  Created by Alexandr Sieroshtan on 9/30/16.
//  Copyright © 2016 Alexandr Sieroshtan. All rights reserved.
//

import Cocoa

extension Notification.Name {
    static let killLauncher = Notification.Name("killLauncher")
}

class AutoLauncherAppDelegate: NSObject {

    struct Constants {
        static let mainAppName = "Battery Life Indicator"
        static let mainAppBundleID = "AlexanderSeroshtan.Battery-Time-Lost"
    }
    
    @objc func terminate() {
        NSApp.terminate(nil)
    }

}

extension AutoLauncherAppDelegate: NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let runningApps = NSWorkspace.shared.runningApplications
        let isRunning = runningApps.contains {
            $0.bundleIdentifier == Constants.mainAppBundleID
        }

        if !isRunning {
            DistributedNotificationCenter.default().addObserver(self,
                                                                selector: #selector(self.terminate),
                                                                name: .killLauncher,
                                                                object: Constants.mainAppBundleID)
            
            let path = Bundle.main.bundlePath as NSString
            var components = path.pathComponents
            components.removeLast()
            components.removeLast()
            components.removeLast()
            components.append("MacOS")
            components.append(Constants.mainAppName)
            
            let newPath = NSString.path(withComponents: components)
            
            NSWorkspace.shared.launchApplication(newPath)
        }
        else {
            self.terminate()
        }
    }

}
