//
//  Battery.swif
//  Battery Time Indicator
//
//  Created by Alexandr Sieroshtan on 9/29/16.
//  Copyright © 2016 Alexandr Sieroshtan. All rights reserved.
//

import Foundation
import IOKit.ps

public enum BatteryCondition: String, Codable {
    case good = "Good"
    case fair = "Fair"
    case poor = "Poor"
}

public enum PowerSourceState: String, Codable {
    case battery
    case acPower
    case unknown
}

public enum TemperatureUnit: String {
    case celsius = "°C"
    case fahrenheit = "°F"
    case kelvin = "°K"
}

struct Battery {
    var currentPercentage = 0
    var maxPercentage = 0
    var currentCharge: Double {
        return Double(currentPercentage) / Double(maxPercentage)
    }

    var condition: BatteryCondition = .good
    var powerSource: PowerSourceState = .unknown
    var timeToFullCharge = 0
    var timeToEmpty = 0
    var isCharged = false
    var isCharging = true
    var isCalculating: Bool {
        if powerSource == .acPower {
            return timeToFullCharge <= 0
        }
        return IOPSGetTimeRemainingEstimate() < 0
    }
    var health = ""

    fileprivate static let IOSERVICE_BATTERY = "AppleSmartBattery"
    fileprivate var service: io_service_t = 0
    
    init() {
        guard
            let blob = IOPSCopyPowerSourcesInfo(),
            let list = IOPSCopyPowerSourcesList(blob.takeRetainedValue()),
            let array = list.takeRetainedValue() as? [Any],
            array.count > 0,
            let dict = array[0] as? NSDictionary
        else {
            return
        }
        
        service = IOServiceGetMatchingService(kIOMasterPortDefault,
                  IOServiceNameMatching(Battery.IOSERVICE_BATTERY))
        currentPercentage = dict[kIOPSCurrentCapacityKey] as? Int ?? 0
        maxPercentage = dict[kIOPSMaxCapacityKey] as? Int ?? 0
        timeToFullCharge = dict[kIOPSTimeToFullChargeKey] as? Int ?? 0
        timeToEmpty = dict[kIOPSTimeToEmptyKey] as? Int ?? 0
        isCharged = dict[kIOPSIsChargedKey] as? Bool ?? false
        isCharging = dict[kIOPSIsChargingKey] as? Bool ?? false
        
        if let value = dict[kIOPSBatteryHealthConditionKey] as? String {
            switch value {
            case kIOPSPoorValue:
                condition = .poor
            case kIOPSFairValue:
                condition = .fair
            default:
                condition = .good
            }
        }

        if let value = dict[kIOPSPowerSourceStateKey] as? String {
            switch value {
            case kIOPSACPowerValue:
                powerSource = .acPower
            case kIOPSBatteryPowerValue:
                powerSource = .battery
            default:
                powerSource = .unknown
            }
        }
        
        if isCharging && powerSource != .acPower {
            isCharging = false
        }
    }
    
    public func cycleCount() -> Int {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "CycleCount" as CFString,
                                                   kCFAllocatorDefault, 0)
        return prop?.takeUnretainedValue() as? Int ?? 0
    }
    
    public func designCycleCount() -> Int {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "DesignCycleCount9C" as CFString,
                                                   kCFAllocatorDefault, 0)
        return prop?.takeUnretainedValue() as? Int ?? 0
    }
    
    public func currentCapacity() -> Int {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "CurrentCapacity" as CFString,
                                                   kCFAllocatorDefault,0)
        return prop?.takeUnretainedValue() as? Int ?? 0
    }
    
    public func maxCapacity() -> Int {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "MaxCapacity" as CFString,
                                                   kCFAllocatorDefault, 0)
        return prop?.takeUnretainedValue() as? Int ?? 0
    }
    
    public func designCapacity() -> Int {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "DesignCapacity" as CFString,
                                                   kCFAllocatorDefault, 0)
        return prop?.takeUnretainedValue() as? Int ?? 0
    }
    
    public func temperature(_ unit: TemperatureUnit = .celsius) -> Double {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "Temperature" as CFString,
                                                   kCFAllocatorDefault, 0)
        
        var temperature = (prop?.takeUnretainedValue() as? Double ?? 0) / 100.0
        
        switch unit {
            case .celsius:
                break
            case .fahrenheit:
                temperature = self.toFahrenheit(temperature)
            case .kelvin:
                temperature = self.toKelvin(temperature)
        }
        
        return ceil(temperature)
    }
    
    public func manufacturer() -> String? {
        let prop = IORegistryEntryCreateCFProperty(service,
                                                   "Manufacturer" as CFString,
                                                   kCFAllocatorDefault, 0)
        return prop?.takeUnretainedValue() as? String
    }
    
    func manufactureDate() -> Date? {
        if let value = IORegistryEntryCreateCFProperty(service, "ManufactureDate" as CFString, kCFAllocatorDefault, 0) {
            let date = value.takeRetainedValue() as! Int

            let day = date & 31
            let month = (date >> 5) & 15
            let year = ((date >> 9) & 127) + 1980

            var components = DateComponents()
            components.calendar = Calendar.current
            components.day = day
            components.month = month
            components.year = year

            return components.date
        }
        return nil
    }
    
    func toFahrenheit(_ temperature: Double) -> Double {
        return (temperature * 1.8) + 32
    }
    
    func toKelvin(_ temperature: Double) -> Double {
        return temperature + 273.15
    }
}
