//
//  AppDelegate.swift
//  Battery Time Indicator
//
//  Created by Alexandr Sieroshtan on 9/28/16.
//  Copyright © 2016 Alexandr Sieroshtan. All rights reserved.
//

import Cocoa
import ServiceManagement

extension Notification.Name {
    static let killLauncher = Notification.Name("killLauncher")
}

class BatteryView : NSView {
    var isCharging: Bool?
    var chargingImage: NSImage?
    var batteryImage: NSImage?
    var inBatteryText: String?
    var nearBatteryText: String?
    var inBatteryTextSize: CGSize?
    var inBatteryTextAttrs: [NSAttributedStringKey: Any]?
    var nearBatteryTextSize: CGSize?
    var nearBatteryTextAttrs: [NSAttributedStringKey: Any]?
    
    override func draw(_ dirtyRect: NSRect) {
        guard let isCharging = isCharging,
            let batteryImage = batteryImage,
            let chargingImage = chargingImage,
            let inBatteryText = inBatteryText,
            let nearBatteryText = nearBatteryText,
            let nearBatteryTextSize = nearBatteryTextSize,
            let nearBatteryTextAttrs = nearBatteryTextAttrs,
            let inBatteryTextSize = inBatteryTextSize,
            let inBatteryTextAttrs = inBatteryTextAttrs else {
                return
        }
        
        var x: CGFloat = 0.0
        
        if isCharging {
            let batteryPosY: CGFloat = (dirtyRect.height - chargingImage.size.height) / 2.0
            chargingImage.draw(in: CGRect(x: x, y: batteryPosY, width: chargingImage.size.width, height: chargingImage.size.height))
            x += chargingImage.size.width + 2
        }
        
        let batteryPosY: CGFloat = (dirtyRect.height - batteryImage.size.height) / 2.0
        batteryImage.draw(in: CGRect(x: x, y: batteryPosY, width: batteryImage.size.width, height: batteryImage.size.height))
        
        inBatteryText.draw(at: CGPoint(x: x + (batteryImage.size.width - inBatteryTextSize.width) / 2.0 - 1.0, y: (dirtyRect.height - inBatteryTextSize.height) / 2.0 + 0.5), withAttributes: inBatteryTextAttrs)
        
        x += batteryImage.size.width + 2
        
        if !UserDefaults.standard.bool(forKey: "compactMode") {
            nearBatteryText.draw(at: CGPoint(x: x, y: (dirtyRect.height - nearBatteryTextSize.height) / 2.0), withAttributes: nearBatteryTextAttrs)
        }
    }
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    struct Constants {
        static let launcherAppBundleID = "com.alex.sieroshtan.battery-life-indicator-helper"
    }
    
    @IBOutlet weak var batteryImage: NSImageView!
    @IBOutlet weak var batteryTitle: NSTextField!
    @IBOutlet weak var title: NSTextField!
    
    @IBOutlet weak var mainView: NSView!

    @IBOutlet weak var chargingViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleViewWidthConstraint: NSLayoutConstraint!
    
    var statusItem: NSStatusItem?
    var batteryView: BatteryView!

    var battery = Battery()
    
    private let powerSourceCallback: IOPowerSourceCallbackType = { context in
        let _self = Unmanaged<AppDelegate>.fromOpaque(UnsafeRawPointer(context!)).takeUnretainedValue()
        _self.battery = Battery()
        _self.update(_self.battery)
        _self.showLowBatteryNotificationIfNeeded(_self.battery)
    }

    var lowBatteryNotificationWasDisplayed: Bool = false

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        self.quitIfBatteryNotFound()
        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
        
        let loop = IOPSNotificationCreateRunLoopSource(powerSourceCallback, Unmanaged.passUnretained(self).toOpaque()).takeUnretainedValue()
        CFRunLoopAddSource(CFRunLoopGetCurrent(), loop, CFRunLoopMode.defaultMode)
        
        guard let button = statusItem?.button else {
            return
        }
        
        button.target = self
        button.action = #selector(self.handleStatusItemButtonAction(sender:))
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: NSStatusBar.system.thickness).isActive = true
        
        batteryView = BatteryView()
        
        mainView.translatesAutoresizingMaskIntoConstraints = false

        button.addSubview(mainView)

        NSLayoutConstraint.activate([
            mainView.leadingAnchor.constraint(equalTo: button.leadingAnchor),
            mainView.trailingAnchor.constraint(equalTo: button.trailingAnchor),
            mainView.topAnchor.constraint(equalTo: button.topAnchor),
            mainView.bottomAnchor.constraint(equalTo: button.bottomAnchor),
        ])

        applySettings()
        self.update(Battery())
    }
    
    @objc func handleStatusItemButtonAction(sender: NSStatusBarButton) {
        let menu = createMenu(self.battery)
        statusItem?.popUpMenu(menu)
    }
    
    func update(_ battery: Battery) {
        var timeLabel = self.batteryTitle
        var percentageLabel = self.title
        
        if UserDefaults.standard.bool(forKey: "showPercentage") {
            timeLabel = self.title
            percentageLabel = self.batteryTitle
        }
        
        var timeRemainingString: String!
        
        if battery.isCharging && battery.timeToFullCharge >= 0 {
            timeRemainingString = battery.timeToFullCharge.readableTimeInMin
        }

        if !battery.isCharging && !battery.isCharged && battery.timeToEmpty >= 0 {
            timeRemainingString = battery.timeToEmpty.readableTimeInMin
        }

        if battery.isCalculating || battery.isCharged {
            timeRemainingString = "~"
        }
        
        if battery.isCharged && battery.timeToFullCharge == 0 {
            batteryImage.image = NSImage(named: NSImage.Name(rawValue: "ChargedBattery"))
        } else {
            batteryImage.image = NSImage(named: NSImage.Name(rawValue: "Battery"))
        }
        
        if let timeRemainingString = timeRemainingString {
            timeLabel?.stringValue = timeRemainingString
        }
        
        if battery.powerSource == .acPower {
            chargingViewWidthConstraint.constant = 10
        } else {
            chargingViewWidthConstraint.constant = 0
        }

        percentageLabel?.stringValue = battery.currentCharge.percentageString
    }

    func applySettings() {
        if UserDefaults.standard.bool(forKey: "compactMode") {
            titleViewWidthConstraint.constant = 0
        } else {
            titleViewWidthConstraint.constant = title.stringValue.size().width + 4.0
        }
    }
    
    func createMenu(_ battery: Battery) -> NSMenu {
        let compactMode = UserDefaults.standard.bool(forKey: "compactMode")
        let showPercentage = UserDefaults.standard.bool(forKey: "showPercentage")
        let launchAtLogin = UserDefaults.standard.bool(forKey: "launchAtLogin")
        
        let statusBarMenu = NSMenu()
        
        if battery.isCharged {
            statusBarMenu.addItem(
                withTitle: NSLocalizedString("Fully Charged", comment: ""),
                action: nil,
                keyEquivalent: "")
        } else if battery.isCalculating {
            var title = ""
            if battery.isCharging {
                title = NSLocalizedString("Calculating Time Until Full…", comment: "")
            } else {
                title = NSLocalizedString("Calculating Time Remaining…", comment: "")
            }
            statusBarMenu.addItem(
                withTitle: title,
                action: nil,
                keyEquivalent: "")
            
        } else {
            if compactMode {
                var title = ""
                if battery.isCharging {
                    title = String.localizedStringWithFormat(NSLocalizedString("%@ Until Full", comment: ""), battery.timeToFullCharge.readableTimeInMin)
                } else {
                    title = String.localizedStringWithFormat(NSLocalizedString("%@ Remaining", comment: ""), battery.timeToEmpty.readableTimeInMin)
                }
                statusBarMenu.addItem(
                    withTitle: title,
                    action: nil,
                    keyEquivalent: "")
            }
        }
        
        let powerSourceString = battery.powerSource == .battery ? NSLocalizedString("Battery", comment: "") : NSLocalizedString("Power Adapter", comment: "")
        
        statusBarMenu.addItem(
            withTitle: String.localizedStringWithFormat(NSLocalizedString("Power Source: %@", comment: ""), powerSourceString),
            action: nil,
            keyEquivalent: "")
        
        if let flags = NSApp.currentEvent?.modifierFlags {
            if flags.contains(.option) {
                var tempUnit: TemperatureUnit = .celsius
                if let unit = CFPreferencesCopyValue("AppleTemperatureUnit" as CFString,
                                                     "Apple Global Domain" as CFString,
                                                     kCFPreferencesCurrentUser,
                                                     kCFPreferencesAnyHost) as? String {
                    if unit == "Kelvin" {
                        tempUnit = .kelvin
                    } else if unit == "Fahrenheit" {
                        tempUnit = .fahrenheit
                    }
                }
                
                var batterySystemInfo: Array<Array<String>> = [
                    [NSLocalizedString("Condition", comment: ""), NSLocalizedString(battery.condition.rawValue, comment: "")],
                    [NSLocalizedString("Cycle Count", comment: ""), String(battery.cycleCount())],
                    [NSLocalizedString("Design Cycle Count", comment: ""), String(battery.designCycleCount())],
                    [NSLocalizedString("Current Capacity", comment: ""), battery.currentCapacity().formatCapacity],
                    [NSLocalizedString("Max Capacity", comment: ""), battery.maxCapacity().formatCapacity],
                    [NSLocalizedString("Design Capacity", comment: ""), battery.designCapacity().formatCapacity],
                    [NSLocalizedString("Temperature", comment: ""), battery.temperature(tempUnit).formatTemp(unit: tempUnit)]
                ]
                
                if let manufacturer = battery.manufacturer() {
                    batterySystemInfo.append([NSLocalizedString("Manufacturer", comment: ""), manufacturer])
                }
                if let manufactureDate = battery.manufactureDate() {
                    batterySystemInfo.append([NSLocalizedString("Manufacture Date", comment: ""), manufactureDate.formatted()])
                }
                
                statusBarMenu.addItem(NSMenuItem.separator())

                for info in batterySystemInfo {
                    statusBarMenu.addItem(
                        withTitle: "\(info[0]): \(info[1])",
                        action: nil,
                        keyEquivalent: "")
                }
            }
        }
        
        statusBarMenu.addItem(NSMenuItem.separator())

        let item0 = NSMenuItem(title: NSLocalizedString("Show Percentage", comment: ""), action: #selector(self.showPercentage(sender:)), keyEquivalent: "")
        item0.state = showPercentage ? .on : .off
        statusBarMenu.addItem(item0)
        
        let item1 = NSMenuItem(title: NSLocalizedString("Compact Mode", comment: ""), action: #selector(self.compactMode(sender:)), keyEquivalent: "")
        item1.state = compactMode ? .on : .off
        statusBarMenu.addItem(item1)
        
        statusBarMenu.addItem(NSMenuItem.separator())

        let item4 = NSMenuItem(title: NSLocalizedString("Launch at Login", comment: ""), action: #selector(self.launchAtLogin(sender:)), keyEquivalent: "")
        item4.state = launchAtLogin ? .on : .off
        statusBarMenu.addItem(item4)
        
        let item2 = NSMenuItem(title: NSLocalizedString("Battery Preferences…", comment: ""), action: #selector(self.openPreferences(sender:)), keyEquivalent: "")
        statusBarMenu.addItem(item2)
        
        let item3 = NSMenuItem(title: NSLocalizedString("Quit", comment: ""), action: #selector(self.quit(sender:)), keyEquivalent: "")
        statusBarMenu.addItem(item3)

        return statusBarMenu
    }

    @objc func showPercentage(sender: NSMenuItem) {
        let newShowPercentage = !UserDefaults.standard.bool(forKey: "showPercentage")
        UserDefaults.standard.set(newShowPercentage, forKey: "showPercentage")
        UserDefaults.standard.synchronize()
        applySettings()
        self.update(self.battery)
    }
    
    @objc func compactMode(sender: NSMenuItem) {
        let newCompactMode = !UserDefaults.standard.bool(forKey: "compactMode")
        UserDefaults.standard.set(newCompactMode, forKey: "compactMode")
        UserDefaults.standard.synchronize()
        applySettings()
        self.update(self.battery)
    }
    
    @objc func launchAtLogin(sender: NSMenuItem) {
        let newlaunchAtLogin = !UserDefaults.standard.bool(forKey: "launchAtLogin")
        UserDefaults.standard.set(newlaunchAtLogin, forKey: "launchAtLogin")
        UserDefaults.standard.synchronize()
        SMLoginItemSetEnabled(Constants.launcherAppBundleID as CFString, newlaunchAtLogin)
    }
    
    @objc func openPreferences(sender: NSMenuItem) {
        if #available(OSX 11.0, *) {
            NSWorkspace.shared.openFile("/System/Library/PreferencePanes/Battery.prefPane")
        } else {
            NSWorkspace.shared.openFile("/System/Library/PreferencePanes/EnergySaver.prefPane")
        }
    }
    
    @objc func quit(sender: NSMenuItem) {
        NSApp.terminate(self)
    }

    func showLowBatteryNotificationIfNeeded(_ battery: Battery) {
        if !lowBatteryNotificationWasDisplayed && battery.currentCharge < 11 && !battery.isCharging {
            lowBatteryNotificationWasDisplayed = true
            self.showLowBatteryNotification(battery)
        }

        if lowBatteryNotificationWasDisplayed && battery.isCharging && battery.currentCharge > 11 {
            lowBatteryNotificationWasDisplayed = false
        }
    }

    func quitIfBatteryNotFound() {
        let blob = IOPSCopyPowerSourcesInfo().takeRetainedValue()
        let sourcesList: NSArray = IOPSCopyPowerSourcesList(blob).takeRetainedValue()

        if (sourcesList.count == 0) {
            let alert = NSAlert()
            alert.messageText = "Battery Life Indicator"
            alert.informativeText = "Sorry, battery is not detected :-("
            alert.addButton(withTitle: "Ok")
            alert.runModal()
            NSApplication.shared.terminate(nil)
        }
    }

    func showLowBatteryNotification(_ battery: Battery) -> Void {
        let notification = NSUserNotification()
        notification.title = NSLocalizedString("Low Battery", comment: "")
        notification.informativeText = NSLocalizedString("Your Mac will sleep soon unless plugged into a power outlet.", comment: "")
        
        notification.soundName = NSUserNotificationDefaultSoundName
        NSUserNotificationCenter.default.deliver(notification)
    }
}
